# Project  Setup 

### Required Software
	1 JRE or JDK 11 or above
	2 Apache Maven 3.6 or above
	3 GIT client
	NB Please ensure all these software are on the path of the terminal you are about to use.
	
### Set up
	1 git clone git@gitlab.com:pubworld/ttsep/bustrack.git
	2 cd 	 bustrack
	3 mvn clean package
	4 Running Options 
		4.1 Using the supplied  wrapper script 
			trackBus rootName stopName  Direction
		4.2 Directly using java
		For example with route 17
		java -jar target\bustrack-0.0.1-SNAPSHOT.jar "Route 17" "Lake St and France Ave" West 
		java -jar target\bustrack-0.0.1-SNAPSHOT.jar 'METRO Orange Line' 'Knox Ave & 76th St Station' South
 
### Validate the results 
	If you are using a linux terminal you may try to validate the result for the route 17 with this direct API
	curl https://svc.metrotransit.org/nextripv2/17/1/LAFR | json_pp | grep departure_text 

		
		