package dev.pa.bustrack.model;

import java.util.List;

public class NextTripResult {
private List<Alert> alerts ;
private List<Departure> departures ;
public List<Alert> getAlerts() {
	return alerts;
}
public void setAlerts(List<Alert> alerts) {
	this.alerts = alerts;
}
public List<Departure> getDepartures() {
	return departures;
}
public void setDepartures(List<Departure> departures) {
	this.departures = departures;
}

}
