package dev.pa.bustrack.model;

public class Alert {

	public String getAlert_text() {
		return alert_text;
	}

	public void setAlert_text(String alert_text) {
		this.alert_text = alert_text;
	}

	private String alert_text;
}
