package dev.pa.bustrack.model;

public class Direction {
	int direction_id;
	String direction_name;
	
	public int getDirection_id() {
		return direction_id;
	}

	public void setDirection_id(int direction_id) {
		this.direction_id = direction_id;
	}

	public String getDirection_name() {
		return direction_name;
	}

	public void setDirection_name(String direction_name) {
		this.direction_name = direction_name;
	}

	@Override
	public String toString() {
		return "Direction [direction_id=" + direction_id + ", direction_name=" + direction_name + "]";
	}
}
