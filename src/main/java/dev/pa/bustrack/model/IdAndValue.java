package dev.pa.bustrack.model;

public class IdAndValue{
	private Integer id; 
	private String address; 
	private String name;
//	String place_code; 
//	String description;
	@Override
	public String toString() {
		return "BusStop [id=" + id + ", address=" + address + ", name=" + name + "]";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}