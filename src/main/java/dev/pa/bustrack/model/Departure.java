package dev.pa.bustrack.model;

public class Departure {
private String departure_text;
private int route_id;
private int  direction_id;
private String direction_text;

public String getDeparture_text() {
	return departure_text;
}
public void setDeparture_text(String departure_text) {
	this.departure_text = departure_text;
}
public int getRoute_id() {
	return route_id;
}
public void setRoute_id(int route_id) {
	this.route_id = route_id;
}
public int getDirection_id() {
	return direction_id;
}
public void setDirection_id(int direction_id) {
	this.direction_id = direction_id;
}
public String getDirection_text() {
	return direction_text;
}
public void setDirection_text(String direction_text) {
	this.direction_text = direction_text;
}

@Override
public String toString() {
	return "Departures [departure_text=" + departure_text + ", route_id=" + route_id + ", direction_id=" + direction_id
			+ ", direction_text=" + direction_text + "]";
}

}
