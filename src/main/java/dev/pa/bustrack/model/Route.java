package dev.pa.bustrack.model;

public class Route {
private String route_id;
public String getRoute_id() {
	return route_id;
}

public void setRoute_id(String route_id) {
	this.route_id = route_id;
}

public String getRoute_label() {
	return route_label;
}

public void setRoute_label(String route_label) {
	this.route_label = route_label;
}

private String route_label;

@Override
public String toString() {
	return "Route [id=" + route_id + ", labels=" + route_label + "]";
}
}
