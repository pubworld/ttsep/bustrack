package dev.pa.bustrack.model;

public class BusStop{
//	private Integer id; 
//	private String address; 
//	private String name;
	String place_code; 
	String description;
	@Override
	public String toString() {
		return "BusStop [id=" + place_code + ", name=" + description + "]";
	}
	public String getPlace_code() {
		return place_code;
	}
	public void setPlace_code(String place_code) {
		this.place_code = place_code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}