package dev.pa.bustrack;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication 
public class TrackApplication {
	private static final Logger log = LoggerFactory.getLogger(TrackApplication.class);
	

	public static void main(String[] args) {
		usage(args);
		SpringApplication.run(TrackApplication.class, args);
		log.debug("MainDone");
	}
	
	static void usage(String[] args) {
		if (args.length < 3 ) {
			System.out.println("Usage trackBus rootName stopName  Direction");
			System.out.println("Example trackBus 'Route 17'  'Lake St and France Ave'  East");
			System.exit(0);
		}		
		//log.debug("Inputs " + Arrays.toString(args));
	}
	
	static void checkversion() {
		if (!System.getProperty("java.version").startsWith("1")  ) {
			System.out.println("Wrong Java version");
			System.exit(0);
		}
		
	}
	


}
