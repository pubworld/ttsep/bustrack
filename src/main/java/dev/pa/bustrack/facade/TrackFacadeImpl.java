package dev.pa.bustrack.facade;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import dev.pa.bustrack.model.BusStop;
import dev.pa.bustrack.model.Departure;
import dev.pa.bustrack.model.Direction;
import dev.pa.bustrack.model.NextTripResult;
import dev.pa.bustrack.model.Route;
import dev.pa.bustrack.service.APIService;

@Service
@PropertySource({"classpath:conf/appconfig.properties"})
public class TrackFacadeImpl implements TrackFacade {
	private static final Logger log = LoggerFactory.getLogger(TrackFacadeImpl.class);
	
	@Autowired
	private APIService api;
	
	@Value("${app.service.errMsg}")
	private String errMsg;

	@Override
	public String getNextBus(String route, String stop, String dirc) {

		String retValue = "Network Error! Please try after some time.";		

		Route reqRoute = null;
		try {
			reqRoute = getRequiredRoute(route);
		} catch (NoSuchElementException e) {
			return "Invalid Route Name, Please enter a proper one";
		} catch (Exception e) {
			System.out.println(errMsg + e.getMessage());
			return retValue;
			
		}

		Direction busDir = null;
		try {
			busDir = getDirection(reqRoute.getRoute_id(), dirc);
		} catch (NoSuchElementException e) {
			return "Invalid Bus Direction value, Please enter a proper one";
		} catch (Exception e) {
			System.out.println(errMsg + e.getMessage());
			return retValue;
		}

		BusStop bstop = null;
		try {
			bstop = getBusStop(reqRoute.getRoute_id(), stop, busDir.getDirection_id());
		} catch (NoSuchElementException e) {
			return "Invalid Bus Stop Name, Please enter a proper one";
		} catch (Exception e) {
			System.out.println(errMsg + e.getMessage());
			return retValue;
		}

		log.debug(" " +reqRoute);
		log.debug(" " +busDir);
		log.debug(" " +bstop);
		retValue = reqRoute.getRoute_id() + " " + bstop.getPlace_code() + " " + busDir.getDirection_id();

		try {
			retValue = getNextBusTripTime(reqRoute.getRoute_id(), bstop.getPlace_code(), busDir.getDirection_id());
		} catch (Exception e) {
			System.out.println(errMsg + e.getMessage());
			return retValue;
		}

		return retValue;
	}

	private Route getRequiredRoute(String route) throws Exception {
		var allRoutes = api.getRoutes();
		Route reqRoute = allRoutes.stream().filter(x -> x.getRoute_label().equalsIgnoreCase(route)).findFirst()
				.orElseThrow();
		return reqRoute;
	}

	private BusStop getBusStop(String rtId, String stop, int dirID) throws Exception {
		var stops = api.getBustopes(rtId, dirID);
		BusStop bstop = stops.stream().filter(x -> x.getDescription().equalsIgnoreCase(stop)).findFirst().orElseThrow();
		return bstop;
	}

	private Direction getDirection(String rtId, String dirc) throws Exception {
		var dtoss = api.getDirection(rtId);
		Direction dirObj = dtoss.stream().filter(x -> x.getDirection_name().startsWith(dirc)).findFirst().orElseThrow();
		return dirObj;
	}

	private String getNextBusTripTime(String rtId, String stop, int dirId) throws Exception {
		String busTime = "Last bus for the day has departed!";
		NextTripResult result = api.getNextBusTime(rtId, stop, dirId);
		if (!ObjectUtils.isEmpty(result)) {
			//List<Alert> alerts = result.getAlerts();
			List<Departure> departs = result.getDepartures();
			if (!ObjectUtils.isEmpty(departs) && departs.size() > 1) {
				busTime = departs.get(0).getDeparture_text();
			}
		}
		return busTime;
	}

}
