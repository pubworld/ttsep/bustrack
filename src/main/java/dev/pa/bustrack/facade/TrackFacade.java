package dev.pa.bustrack.facade;

public interface TrackFacade {
  public String getNextBus(String route,String stop, String dirc );
}
