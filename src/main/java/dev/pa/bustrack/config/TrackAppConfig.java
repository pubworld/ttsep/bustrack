package dev.pa.bustrack.config;

import java.time.Duration;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import dev.pa.bustrack.facade.TrackFacade;

@Configuration
public class TrackAppConfig {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		builder.setConnectTimeout(Duration.ofSeconds(25));
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate, TrackFacade api) throws Exception {
		return args -> {
//			System.out.println("Command Args " +Arrays.toString(args));
			System.out.println("Next Bus -> " + api.getNextBus(args[0], args[1], args[2]));
		};
	}

	/*
	 * @Bean public CommandLineRunner myFunRun(RestTemplate restTemplate) throws
	 * Exception { return (parms) ->
	 * System.out.println("Command 2 Done "+Arrays.toString(parms) ); }
	 */

//	   @Bean
//	   @Primary
//	    public RestTemplate newRestTemplate(RestTemplate restTemplate) {
//		   int TIMEOUT =100000; // (int) Duration.ofSeconds(25) ; //TimeUnit.SECONDS.toMillis(10);
//	        SimpleClientHttpRequestFactory factory =
//	                (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
//
//	        factory.setReadTimeout(TIMEOUT);
//	        factory.setConnectTimeout(TIMEOUT);
//
//	        return restTemplate;
//	    }
	
	
/*	
	@Bean
	public CommandLineRunner localApirun(RestTemplate restTemplate) throws Exception {
		String strUrl="http://localhost:3004/users/";

		return args -> {
//			BusStop quote = restTemplate.getForObject(strUrl, BusStop.class);
//			String toPrint= quote.toString();
			//log.info(quote.toString());
			//List<BusStop quote = restTemplate.getForObject(strUrl, BusStop.class);
			ResponseEntity<IdAndValue[]> responseEntity = restTemplate.getForEntity(strUrl, IdAndValue[].class);
			IdAndValue[] objects = responseEntity.getBody();
			String toPrint=Arrays.toString(objects);
			log.info("HOL");
			System.out.println(toPrint);
			System.out.println("Command Done " +Arrays.toString(args));
		};
	} */
	


}
