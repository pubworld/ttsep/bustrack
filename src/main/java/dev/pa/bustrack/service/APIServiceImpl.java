package dev.pa.bustrack.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import dev.pa.bustrack.model.BusStop;
import dev.pa.bustrack.model.Direction;
import dev.pa.bustrack.model.NextTripResult;
import dev.pa.bustrack.model.Route;

@Service
@PropertySource({"classpath:conf/appconfig.properties"})
public class APIServiceImpl implements APIService {
	@Value("${svc.api.routeAPI}")
	String routeAPI;
	@Value("${svc.api.stopsAPI}")
	String stopsAPI;
	@Value("${svc.api.dirAPI}")
	String dirAPI;
	@Value("${svc.api.nextBusAPI}")
	String nextBusAPI;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public List<Route> getRoutes() {
		ResponseEntity<Route[]> responseEntity = restTemplate.getForEntity(routeAPI, Route[].class);
		Route[] objects = responseEntity.getBody();
		return Arrays.asList(objects);
	}

	@Override
	public List<BusStop> getBustopes(String rtId, int dirId) {
		var urlApi = stopsAPI + rtId + "/" + dirId;
		ResponseEntity<BusStop[]> responseEntity = restTemplate.getForEntity(urlApi, BusStop[].class);
		BusStop[] objects = responseEntity.getBody();
		return Arrays.asList(objects);
	}

	@Override
	public List<Direction> getDirection(String rtId) {
		var urlApi = dirAPI + rtId;
		ResponseEntity<Direction[]> responseEntity = restTemplate.getForEntity(urlApi, Direction[].class);
		Direction[] objects = responseEntity.getBody();
		return Arrays.asList(objects);
	}

	@Override
	public NextTripResult getNextBusTime(String rtId, String stopCode, int dirId) {
		var urlApi = nextBusAPI + rtId + "/" + dirId + "/" + stopCode;
		ResponseEntity<NextTripResult> responseEntity = restTemplate.getForEntity(urlApi, NextTripResult.class);
		NextTripResult object = responseEntity.getBody();
		return object;
	}

}
