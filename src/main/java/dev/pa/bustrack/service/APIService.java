package dev.pa.bustrack.service;

import java.util.List;

import dev.pa.bustrack.model.BusStop;
import dev.pa.bustrack.model.Direction;
import dev.pa.bustrack.model.NextTripResult;
import dev.pa.bustrack.model.Route;

public interface APIService {
	public List<Route> getRoutes();

	public List<BusStop> getBustopes(String rtId, int dirId);

	public List<Direction> getDirection(String rtId);

	public NextTripResult getNextBusTime(String rtId, String stopCode, int dirId);

}
